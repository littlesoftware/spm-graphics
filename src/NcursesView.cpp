#include "NcursesView.h"
#include "utils.h"
#include "Logger.h"
#include <ncurses.h>
#include <sstream>
#include <algorithm>
#include <thread>
#include <chrono>

NcursesView::NcursesView(spm::ServiceManager &manager):
    BaseService("ncurses_view"),
    m_manager(&manager),
    m_running(false),
    m_menuManager(this)
{
    addSupportedType("logic");
}

NcursesView::~NcursesView()
{
    if(m_isInit)
        cleanNcurses();
}

void NcursesView::setMenu(const spm::Menu &menu, MenuType type)
{
    m_menuManager.addMenu(type, menu);
}

void NcursesView::initialize()
{
    initNcurses();
    getmaxyx(stdscr, m_screenHeight, m_screenWidth);
    m_menuManager.showMenu(MenuType::MAIN);
}

bool NcursesView::tick()
{
    getScreen();
    int key;
    while((key = getch()) != ERR)
    {
        onKeyboard(key);
    }

    onDrawArea();
    onDrawMenu();

    update_panels();
    doupdate();
    refresh();

    return m_running;
}

void NcursesView::action(const std::string &type, const spm::Variant &data)
{
    // only view actions
    const spm::Variant::Dict& args = data.toDict();
    const std::string& event = args.at("event").toString();
    if(event == "area")
    {
        m_width = args.at("width");
        m_height = args.at("height");
        m_place = reinterpret_cast<const int32_t*>(args.at("place").toData().data());
    }
}

void NcursesView::initNcurses()
{
    initscr();
    m_isInit = true;
    raw();
    nonl();
    noecho();
    curs_set(0);
    keypad(stdscr, true);
    scrollok(stdscr, true);
    nodelay(stdscr, true);
    if(!has_colors())
    {
        endwin();
        throw spm::exceptf("terminal does not support colors");
    }
    start_color();
    use_default_colors();

    init_pair( Color::BACKGROUND, COLOR_WHITE, COLOR_BLACK );
    init_pair( Color::MENU, COLOR_WHITE, COLOR_BLACK );
    init_pair( Color::MENU_SELECTED, COLOR_BLACK, COLOR_WHITE );

}

void NcursesView::cleanNcurses()
{
    endwin();
}

void NcursesView::createMenu(const spm::Menu *menu)
{
    NWindowMenu window;
    int width, height;

    window.window = nullptr;
    window.menu = menu;
    window.panel = nullptr;
    window.index = 0;
    createWindow( window, width, height );
    drawItemsWindow( window );
    top_panel(window.panel);
    m_windows.push_back(window);
}

void NcursesView::getScreen()
{
    int newWidth, newHeight;
    getmaxyx(stdscr, newHeight, newWidth);
    m_screenResized = (newWidth != m_screenWidth) || (newHeight != m_screenHeight);
    if(m_screenResized)
    {
        spm::Logger::sysTrace(spm::stringf("resize to %dx%d", newWidth, newHeight));
        m_screenWidth = newWidth;
        m_screenHeight = newHeight;
    }
}

void NcursesView::createWindow(NcursesView::NWindowMenu &windowMenu, int &width, int &height)
{
    width =  std::min<int>(30, m_screenWidth);
    bool isEdit = windowMenu.menu->isVariable() && windowMenu.menu->isEmpty();
    height = isEdit ? 4 :
                std::min<int>(windowMenu.menu->getSize()+2, m_screenHeight);
    WINDOW* window;

    window = newwin(height, width, 0, 0);

    spm::Logger::sysTrace(spm::stringf("new overlay 0x%08X (old 0x%08X)", window, windowMenu.window));

    if(windowMenu.panel != nullptr)
        replace_panel( windowMenu.panel, window );
    else
        windowMenu.panel = new_panel( window );

    if(windowMenu.window != nullptr)
        delwin(windowMenu.window);
    windowMenu.window = window;

    moveWindow(windowMenu, width, height);
    // if edit
    if(isEdit)
    {
        windowMenu.field[0] = new_field(1, 28, 2,1,0,0);
        windowMenu.field[1] = nullptr;
        set_field_back(windowMenu.field[0], COLOR_PAIR(MENU_SELECTED) | A_BOLD);
        // add to window
        windowMenu.form = new_form(windowMenu.field);
        set_form_sub(windowMenu.form, windowMenu.window);
        post_form(windowMenu.form);
    }

    // draw title and box
    wbkgd(window, COLOR_PAIR(Color::MENU));
    box(window, 0, 0);
    const std::string& title = windowMenu.menu->getName();
    int titleLength = spm::stringLengthByUtf8(title);
    wattron(windowMenu.window, A_BOLD);
    mvwprintw(windowMenu.window, 0, (width-titleLength-2)/2, spm::stringf(" %s ", title.c_str()).c_str());
    wattroff(windowMenu.window, A_BOLD);
    if(isEdit)
    {
        mvwaddwstr(windowMenu.window, 1, 1, L"Enter value:");
    }

}

void NcursesView::moveWindow(NcursesView::NWindowMenu &window, int width, int height)
{
    move_panel(window.panel, (m_screenHeight-height)/2, (m_screenWidth-width)/2);
}

void NcursesView::printToString(std::wstring &buffer, const std::string &text, bool toLeft) const
{

}

void NcursesView::drawItemsWindow(NcursesView::NWindowMenu &window)
{
    if(window.menu->isVariable() && window.menu->isEmpty())
        return;
    int height, width, firstItem, drawItems=0;
    getmaxyx(window.window, height, width);
    height -= 2;
    width  -= 2;
    //
    int line = 0;
    firstItem = std::min(std::max(window.index-height/2,0), window.menu->getSize()-height);
    spm::Logger::sysTrace(spm::stringf("draw overlay on 0x%08X", window.window));
    for(auto& item : *window.menu)
    {
        if(line >= firstItem)
        {
            if(drawItems >= height)
                break;
            std::wstring buffer = spm::wcast(item.getName());
            int nameLength = item.getName().size();
            int valueLength = 0;
            buffer.resize(width, ' ');
            if(item.isVariable())
            {
                std::wstring value = spm::wcast(item.getValue());
                if(value.size()+2 > width/2)
                {
                    value.resize(width/2-3);
                    value += wchar_t(8230);
                }
                value = L"[" + value + L"]";
                valueLength = value.size();
                buffer = buffer.replace(width-valueLength, valueLength, value);
            }
            if(nameLength+valueLength > width)
            {
                buffer[width-valueLength-1] = wchar_t(8230);
            }
            if(window.index == line)
            {
                wattron(window.window, COLOR_PAIR(Color::MENU_SELECTED) );
                wattron(window.window, A_BOLD);
                mvwaddwstr(window.window, drawItems+1, 1, buffer.c_str() );
                wattroff(window.window, A_BOLD);
                wattroff(window.window, COLOR_PAIR(Color::MENU_SELECTED) );
            }
            else
                mvwaddwstr(window.window, drawItems+1, 1, buffer.c_str() );
            drawItems++;
        }
        line++;
    }
}

void NcursesView::closeMenu()
{
    // get top from stack window
    NWindowMenu& window = m_windows.back();
    del_panel(window.panel);
    delwin(window.window);
    doupdate();
    // remove last menu
    m_windows.pop_back();
}

void NcursesView::onKeyboard(int key)
{
    spm::Logger::sysTrace(spm::stringf("key = %d", key));
    // has menu?
    if(!m_windows.empty())
    {
        NWindowMenu& window = m_windows.back();
        if(window.menu->isVariable() && window.menu->isEmpty())
        {
            switch (key) {
            case 13:
                form_driver(window.form, REQ_VALIDATION);
                break;
            case 27:
                m_menuManager.cancelMenu();
                break;
            case KEY_BACKSPACE:
                form_driver(window.form, REQ_DEL_PREV);
                break;
            default:
                form_driver(window.form, key);
                break;
            }
            return;
        }
        switch (key) {
        case KEY_UP:
        case 'p':
            if(window.index <= 0)
                break;
            window.index--;
            drawItemsWindow(window);
            break;
        case KEY_DOWN:
        case 'n':
            if(window.menu == nullptr ||  window.index+1 >= window.menu->getSize())
                break;
            window.index++;
            drawItemsWindow(window);
            break;
        case 13:  // Enter
        case ' ':
        case KEY_RIGHT:
            m_menuManager.selectItemMenu(window.index);
            break;
        case 27: // Esc
        case KEY_BACKSPACE:
        case 'q':
        case KEY_LEFT:
            m_menuManager.cancelMenu();
            break;
        default:
            break;
        }
    }
    else
    {
        switch (key) {
        case 27:  // Esc (delay some seconds becouse emulate META key)
            m_menuManager.showMenu(MenuType::PAUSE);
            break;
        default:
            break;
        }
    }
}

void NcursesView::onDrawArea()
{
    if(m_place == nullptr)
        return;
    int cur = 0;
    for(int y=0; y<m_height; y++)
    {
        for(int x=0; x<m_width; x++, cur++)
        {
            if(m_place[cur] == 1)
            {
                attron( COLOR_PAIR(1) );
                mvwprintw(stdscr, y, x*2, "%2d", m_place[cur]);
                attroff( COLOR_PAIR(1) );
            }
            else
            {
                attron( COLOR_PAIR(2) );
                mvwprintw(stdscr, y, x*2, "%2d", m_place[cur]);
            }
        }
    }
}

void NcursesView::onDrawMenu()
{
    if(m_screenResized)
    {
        clear();
        for(auto& window : m_windows)
        {
            int width, height;
            getmaxyx(window.window, height, width);
            // if screen less then window
            if(width >= m_screenWidth || height >= m_screenHeight)
            {
                createWindow(window, width, height);
                drawItemsWindow(window);
            }
            else
                moveWindow(window, width, height);
        }
    }
}

void NcursesView::actionExit()
{
    m_running = false;
}

void NcursesView::onOpenMenu(const spm::Menu *menu)
{
    if(menu == nullptr)
        return;
    createMenu(menu);
    m_running = !m_windows.empty();
}

void NcursesView::onCloseMenu()
{
    closeMenu();
}

void NcursesView::onMenuAction(uint32_t action)
{
    switch (action) {
    case Action::EXIT:
        m_running = false;
        m_manager->performAction("view", {{"event", "exit"}});
        break;
    default:
        break;
    }
}

