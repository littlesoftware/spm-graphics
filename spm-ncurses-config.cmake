option(BUILD_SPM_NCURSES "Build ncurses graphics" ON)

if(BUILD_SPM_NCURSES)
    include_directories("${CMAKE_CURRENT_LIST_DIR}/include")
    add_definitions(-DSPM_NCURSES)
endif()
