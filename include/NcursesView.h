#ifndef NCURSESVIEW_H
#define NCURSESVIEW_H

#include "BaseService.h"
#include "ServiceManager.h"
#include "Menu.h"
#include "MenuManager.h"
#include <IMenuListener.h>
#include <ncurses.h>
#include <panel.h>
#include <form.h>
#include <list>
#include <stack>

class NcursesView: public spm::BaseService,
        public spm::IMenuListener
{
public:
    enum MenuType {
        MAIN,
        PAUSE
    };

    enum Action {
        EXIT = spm::MenuManager::Action::OTHER
    };

    explicit NcursesView(spm::ServiceManager& manager);
    virtual ~NcursesView();

    void setMenu(const spm::Menu& menu, MenuType type);

private:
    virtual void initialize() override final;
    virtual bool tick() override final;
    virtual void action(const std::string& type, const spm::Variant& data) override final;

private:
    // -------------------
    // PRIVATE TYPES
    // -------------------
    struct NWindowMenu
    {
        WINDOW *window;
        PANEL  *panel;
        FIELD  *field[2];
        FORM   *form;
        const spm::Menu* menu;
        int index;
    };
    typedef std::list<NWindowMenu> NWindowMenuList;

    enum Color
    {
        BACKGROUND = 1,
        MENU,
        MENU_SELECTED,
    };

    // -------------------
    // PRIVATE METHODS
    // -------------------
    void initNcurses();     // initialize NCurses
    void cleanNcurses();    // clean NCurses
    void createMenu(const spm::Menu *menu);
    inline void getScreen();
    inline void createWindow(NWindowMenu& windowMenu, int &width, int &height);
    inline void moveWindow(NWindowMenu& window, int width, int height);
    inline void printToString(std::wstring& buffer, const std::string& text, bool toLeft = true) const;
    inline void drawItemsWindow(NWindowMenu& window);
    void closeMenu();
    // manipulations keyboard
    inline void onKeyboard(int key);
    inline void onDrawArea();
    inline void onDrawMenu();
    // actions
    inline void actionExit();      // when some logic to exit

    // open menu
    virtual void onOpenMenu(const spm::Menu* menu) override final;
    // close menu
    virtual void onCloseMenu() override final;
    // action menu
    virtual void onMenuAction(uint32_t action) override final;

    // -------------------
    // MEMORY DATA
    // -------------------
    spm::ServiceManager *m_manager;
    bool                 m_running;
    const int32_t       *m_place {nullptr};
    int                  m_width, m_height;
    bool                 m_isInit {false};
    spm::MenuManager     m_menuManager;
    // ncurses components
    int                  m_screenWidth;     // screen width
    int                  m_screenHeight;    // screen height
    bool                 m_screenResized;   // has new size screen
    NWindowMenuList      m_windows;
};

#endif // NCURSESVIEW_H
